import csv
from csv import writer
from csv import reader
import os
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys



#new_dic = {'dot': 'list value' }

options = webdriver.ChromeOptions()
options.add_argument("--incognito")


driver = webdriver.Chrome("C:\chrome\chromedriver",options=options)


BASEDDIR = os.getcwd()

in_csv = 'arizona_data.csv'
out_csv = 'arizona_pull_fn.csv'

csv_file = os.path.join(BASEDDIR, 'csv', in_csv)

new_csv_file = os.path.join(BASEDDIR, 'output_csv',out_csv)

#csv_file = os.path.join(BASEDDIR, 'csv', 'data_MO.csv')
#new_csv_file = os.path.join(BASEDDIR, 'csv','newfile.csv')

#chromepath = 


#Open csv




#read Line and get Dot

# open browser

# search Dot

def searchDot(dot_number):
	#driver = webdriver.Chrome('C:\chrome\chromedriver')
	#time.sleep(1.5)
	driver.get("https://safer.fmcsa.dot.gov/CompanySnapshot.aspx")
	search_bar = driver.find_element_by_id("4")
	search_bar.clear()
	search_bar.send_keys(str(dot_number))
	search_bar.send_keys(Keys.RETURN)

	#result_page = search_bar.find_elements_by css_selector('/html/body/p/table/tbody/tr[2]/td/table/tbody/tr[2]/td/center[1]/table/tbody/tr[3]/td[1]')

	# result_att_val = search_bar

	status_text = "Not in system anymore"

	try:
		is_active_path = driver.find_element_by_xpath('//html/body/p/table/tbody/tr[2]/td/table/tbody/tr[2]/td/center[1]/table/tbody/tr[3]/td[1]')

		status_text = is_active_path.text

		#print(is_active_path.text)

		return status_text


	except Exception as e:
		print(e)
		return status_text




def opencsv():



	with open(csv_file, 'r') as read_obj, open(new_csv_file, 'w', newline='') as write_obj:
		line_count = 0
		csv_reader = reader(read_obj)

		csv_writer = writer(write_obj)

		for row in csv_reader:

			if line_count == 0:

				#print(f'Column names are{", ".join(row)}'+ '    IF IS PRINTINGS')
				row.append("status_check")
				csv_writer.writerow(row)
				line_count +=1
			else:
				text=searchDot(row[0])
				#print(f'\t{row[0]} ' + '   else is printing')
				print (text + "  " +str(line_count))
				row.append(text)
				csv_writer.writerow(row)
				#time.sleep(2)
				line_count += 1





if __name__ == '__main__':
	opencsv()


