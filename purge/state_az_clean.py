import csv
from csv import writer
from csv import reader
import os
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys



#new_dic = {'dot': 'list value' }

options = webdriver.ChromeOptions()
options.add_argument("--incognito")


driver = webdriver.Chrome("C:\chrome\chromedriver",options=options)


BASEDDIR = os.getcwd()

WED_ADDRESS = "https://ecorp.azcc.gov/EntitySearch/Index"
SEARCH_BOX = "quickSearch_BusinessName"

SEARCH_BUTTON = "/html/body/div[1]/div/div[1]/div/div/button"

FIRST_RESULT_CLICK = "/html/body/div[1]/div/div/div[3]/div[1]/div[2]/table[1]/tbody/tr/td[2]/a"

OWNER_NAME = "/html/body/div[1]/div/div[21]/table[1]/tbody/tr/td[2]"

in_csv = 'az_part_2.csv'
out_csv = 'az_pull_own_part_2.csv'

csv_file = os.path.join(BASEDDIR, 'remove_csv', in_csv)

new_csv_file = os.path.join(BASEDDIR, 'owner_name_csv',out_csv)



def searchBusinessName(business_name):
	#driver = webdriver.Chrome('C:\chrome\chromedriver')
	#time.sleep(1.5)
	driver.get(WED_ADDRESS)
	search_bar = driver.find_element_by_id(SEARCH_BOX)
	search_bar.clear()
	search_bar.send_keys(str(business_name))
	#time.sleep(2)
	search_bar.send_keys(Keys.RETURN)

	#start_search = driver.find_element_by_xpath(SEARCH_BUTTON)
	#start_search.click()
	time.sleep(2)

	status_text = "No business found"


	try:
		time.sleep(2)
		result_select_first = driver.find_element_by_xpath(FIRST_RESULT_CLICK)

		result_select_first.click()



		
		time.sleep(1)

	
	except Exception as e:

		print (e)

		return  status_text




	try: 
		time.sleep(2)
		business_owner_name = driver.find_element_by_xpath(OWNER_NAME)

		owner_text = business_owner_name.text

		return owner_text


	except Exception as e:
		print(e)
		return status_text




def opencsv():



	with open(csv_file, 'r') as read_obj, open(new_csv_file, 'w', newline='') as write_obj:
		line_count = 0
		csv_reader = reader(read_obj)

		csv_writer = writer(write_obj)

		for row in csv_reader:

			if line_count == 0:

				#print(f'Column names are{", ".join(row)}'+ '    IF IS PRINTINGS')
				row.append("owner_name")
				csv_writer.writerow(row)
				line_count +=1
			else:
				text=searchBusinessName(row[1])
				#print(f'\t{row[0]} ' + '   else is printing')
				print (text + "  " +str(line_count))
				row.append(text)
				csv_writer.writerow(row)
				#time.sleep(2)
				line_count += 1





if __name__ == '__main__':
	opencsv()


