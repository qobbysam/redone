import csv
from csv import writer
from csv import reader
import os
import time


BASEDIR = os.getcwd()

csv_file_name = "arizona_pull_fn.csv"
new_csv_file_name = "arizona_pull_cl.csv"

csv_file_path = os.path.join(BASEDIR, "output_csv", csv_file_name)
new_csv_file_path = os.path.join(BASEDIR, "remove_csv", new_csv_file_name)


def checkactive(status):
	if status == "AUTHORIZED FOR Property":
		return True

	else:
		return False


def opencsv():



	with open(csv_file_path, 'r') as read_obj, open(new_csv_file_path, 'w', newline='') as write_obj:
		line_count = 0
		csv_reader = reader(read_obj)

		csv_writer = writer(write_obj)

		for row in csv_reader:

			if line_count == 0:

				# #print(f'Column names are{", ".join(row)}'+ '    IF IS PRINTINGS')
				# row.append("")
				csv_writer.writerow(row)
				line_count +=1

			else:
				status = checkactive(row[-1])
				
				if status:

					csv_writer.writerow(row)
					print(line_count)
					#time.sleep(2)
				
				line_count += 1





if __name__ == '__main__':
	opencsv()