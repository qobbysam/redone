import csv
from csv import writer
from csv import reader
import os
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys



#new_dic = {'dot': 'list value' }

options = webdriver.ChromeOptions()
options.add_argument("--incognito")


driver = webdriver.Chrome("C:\chrome\chromedriver",options=options)


BASEDDIR = os.getcwd()

in_csv = 'oh_pull.csv'
out_csv = 'oh_pull_fn.csv'

csv_file = os.path.join(BASEDDIR, 'oh_csv', in_csv)

new_csv_file = os.path.join(BASEDDIR, 'oh_csv',out_csv)



def searchBusinessName(business_name):
	#driver = webdriver.Chrome('C:\chrome\chromedriver')
	#time.sleep(1.5)
	driver.get("https://businesssearch.ohiosos.gov/")
	search_bar = driver.find_element_by_id("bSearch")
	search_bar.clear()
	search_bar.send_keys(str(business_name))

	start_search = driver.find_element_by_xpath("/html/body/main/div[1]/div[1]/div[2]/fieldset/section[1]/div/div[3]/div[1]/input[1]")
	start_search.click()
	time.sleep(2)

	status_text = "No business found"


	try:
		time.sleep(2)
		result_select_first = driver.find_element_by_xpath("/html/body/main/div[1]/div[2]/div/div[3]/table/tbody/tr/td[10]/a")

		result_select_first.click()



		
		time.sleep(2)

	
	except Exception as e:

		print (e)

		return  status_text




	try: 
		time.sleep(2)
		business_owner_name = driver.find_element_by_xpath('/html/body/div[1]/div[2]/div/div/div/section[1]/div/p[1]')

		owner_text = business_owner_name.text

		return owner_text


	except Exception as e:
		print(e)
		return status_text




def opencsv():



	with open(csv_file, 'r') as read_obj, open(new_csv_file, 'w', newline='') as write_obj:
		line_count = 0
		csv_reader = reader(read_obj)

		csv_writer = writer(write_obj)

		for row in csv_reader:

			if line_count == 0:

				#print(f'Column names are{", ".join(row)}'+ '    IF IS PRINTINGS')
				#row.append("status_check")
				#csv_writer.writerow(row)
				line_count +=1
			else:
				text=searchBusinessName(row[1])
				#print(f'\t{row[0]} ' + '   else is printing')
				print (text + "  " +str(line_count))
				row.append(text)
				csv_writer.writerow(row)
				#time.sleep(2)
				line_count += 1





if __name__ == '__main__':
	opencsv()


