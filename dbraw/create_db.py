#!/usr/bin/python
import psycopg2

from config import config


def create_tables():
	params = config()

	conn = psycopg2.connect(**params)
	try: 


		cur = conn.cursor()


		create_table_query = ''' CREATE TABLE interstate ( 
					DOT_NUMBER INT PRIMARY KEY NOT NULL,
								
					LEGAL_NAME		TEXT,
					DBA_NAME		TEXT ,
					PHY_STREET		TEXT,	
					PHY_CITY		TEXT,
					PHY_STATE		TEXT,
					PHY_ZIP		varchar(255),
					PHY_COUNTRY		TEXT,
					MAILING_STREET		TEXT,
					MAILING_CITY		TEXT,
					MAILING_STATE		TEXT,
					MAILING_ZIP		varchar(255),
					MAILING_COUNTRY		TEXT,
					TELEPHONE		TEXT,
					FAX		TEXT,
					EMAIL_ADDRESS		TEXT,
					MCS150_DATE		DATE,
					MCS150_MILEAGE		TEXT,
					MCS150_MILEAGE_YEAR		TEXT,
					ADD_DATE		DATE,
					OIC_STATE		TEXT,
					NBR_POWER_UNIT		INT,	
					DRIVER_TOTAL	TEXT
					
					
					

								);

							 '''
		

		cur.execute(create_table_query)

		conn.commit()
		print ("table sicess fi;;u created")

	except (Exception, psycopg2.DatabaseError) as error:
		print(error)


	finally:

		if conn is not None:

			conn.close()

if __name__ == '__main__':
	create_tables() 