#!/usr/bin/python
import psycopg2

from config import config


def create_tables():
	params = config()

	conn = psycopg2.connect(**params)
	try: 


		cur = conn.cursor()


		create_table_query = ''' CREATE TABLE inspections ( 

					UNIQUE_ID BIGINT PRIMARY KEY NOT NULL,								
					REPORT_NUMBER		BIGINT,
					REPORT_STATE		TEXT ,
					DOT_NUMBER		INT,	
					INSP_DATE		DATE,
					INSP_LEVEL_ID		INT,
					COUNTY_CODE_STATE		TEXT,
					TIME_WEIGHT		INT,
					DRIVER_OOS_TOTAL		INT,
					VEHICLE_OOS_TOTAL		INT,
					TOTAL_HAZMAT_SENT		INT,
					OOS_TOTAL		INT,
					HAZMAT_OOS_TOTAL		INT,
					HAZMAT_PLACECARD_REQ		VARCHAR(5),
					UNIT_TYPE_DESC		TEXT,
					UNIT_MAKE		TEXT,
					UNIT_LICENSE		TEXT,
					UNIT_LICENSE_STATE		TEXT,
					VIN		TEXT,
					UNIT_DECAL_NUMBER		TEXT,
					UNIT_TYPE_DESC2		TEXT,
					UNIT_MAKE2		TEXT,	
					UNIT_LICENSE2	TEXT,
					UNIT_LICENSE_STATE2 	TEXT,
					VIN2 	TEXT,
					UNIT_DECAL_NUMBER2 	TEXT,
					UNSAFE_INSP 	VARCHAR(5),
					FATIGUED_INSP 	VARCHAR(5),
					DR_FITNESS_INSP 	VARCHAR(5),
					SUBT_ALCOHOL_INSP 	VARCHAR(5),
					VH_MAINT_INSP 	VARCHAR(5),
					HM_INSP 	VARCHAR(5),
					BASIC_VIOL 		INT,
					UNSAFE_VIOL 	INT,
					FATIGUED_VIOL 	INT,
					DR_FITNESS_VIOL 	INT,
					SUBT_ALCOHOL_VIOL 	INT,
					VH_MAINT_VOL 	INT,
					HM_VIOL 	INT

					
					
					

								);

							 '''
		

		cur.execute(create_table_query)

		conn.commit()
		print ("table sicess fi;;u created")

	except (Exception, psycopg2.DatabaseError) as error:
		print(error)


	finally:

		if conn is not None:

			conn.close()

if __name__ == '__main__':
	create_tables() 