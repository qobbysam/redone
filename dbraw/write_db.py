#!/usr/bin/python
import psycopg2
import os
from datetime import datetime
import fileinput
from csv import reader

from config import config
from helpers.cleanops import CleanDirty

BASE_DIR = os.getcwd()

FILE_PATH = os.path.join(BASE_DIR,'FMCSA')

Purge = CleanDirty()
purge_string = Purge.clean_string
purge_number = Purge.clean_number
purge_date = Purge.clean_date



def create_tables():



	try: 
		params = config()

		conn = psycopg2.connect(**params)

		cur = conn.cursor()

		
		insert_into_db = """

							INSERT INTO interstate VALUES 

							(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);

						  """
		
		with open(FILE_PATH+"/FMCSA_CENSUS1_2020Mar.txt") as fileobject:
			
			for line in fileobject:

				n = line.rstrip()
				m = n.split(",")

				ind = m

				print (ind)

				#print(ind[0])

				lks = ind[0]

				yp = purge_number(lks)

				# print (yp)
				ot = purge_number(ind[0])
				ab = purge_string(ind[1])
				ac = purge_string(ind[2])
				yr = purge_string(ind[5])
				yt = purge_string(ind[3])							
				av = purge_string(ind[6])
				aw = purge_string(ind[7])
				ar = purge_string(ind[8])
				aq = purge_string(ind[9])
				ay = purge_string(ind[10])
				nu = purge_string(ind[11])
				xe = purge_string(ind[12])
				cw = purge_string(ind[13])
				vr = purge_string(ind[14])
				ve = purge_string(ind[15])
				bt = purge_string(ind[16])
				br = purge_string(ind[17])
				bk = purge_string(ind[18])
				bj = purge_date(ind[19])
				bs = purge_string(ind[20])
				np = purge_string(ind[21])
				by = purge_date(ind[22])
				hp = purge_string(ind[23])
				ho = purge_number(ind[24])
				hu = purge_string(ind[25]) 


				data = (ot,ab,ac,av,aw,ar,aq,ay,nu,xe,cw,vr,ve,bt,br,bk,bj,bs,np,by,hp,ho,hu)

				#cur.execute(insert_into_db, data)
				
				
				#print (data)
				if yt == 'A':
					#print "can do a now"
					print (data)
					cur.execute(insert_into_db, data)
				# else:
				# 	print "not doing a now"

				conn.commit()


	except (Exception, psycopg2.DatabaseError) as error:
		print(error)


	finally:
		if conn is not None:
			conn.close()

if __name__ == '__main__':
	create_tables() 





# 					ID INT PRIMARY KEY NOT NULL,
# 					LEGAL_NAME		TEXT,
# 					DBA_NAME		TEXT ,
# 					PHY_STREET		TEXT,	
# 					PHY_CITY		TEXT,
# 					PHY_STATE		TEXT,
# 					PHY_ZIP		REAL,
# 					PHY_COUNTRY		TEXT,
# 					MAILING_STREET		TEXT,
# 					MAILING_CITY		TEXT,
# 					MAILING_STATE		TEXT,
# 					MAILING_ZIP		REAL,
# 					MAILING_COUNTRY		TEXT,
# 					TELEPHONE		TEXT,
# 					FAX		TEXT,
# 					EMAIL_ADDRESS		TEXT,
# 					MCS150_DATE		TEXT,
# 					MCS150_MILEAGE		TEXT,
# 					MCS150_MILEAGE_YEAR		TEXT,
# 					ADD_DATE		TEXT,
# 					OIC_STATE		TEXT,
# 					NBR_POWER_UNIT		REAL,	
# 					DRIVER_TOTAL	REAL,