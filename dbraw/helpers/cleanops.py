#!/bin/usr
import os
import math

from datetime import datetime
import fileinput
#from csv import reader

class CleanDirty():
	"""docstring for CleanDirty"""
	# def __init__(self, arg):
	# 	super(CleanDirty, self).__init__()
	# 	self.arg = arg

	empty = '""'

	def clean_string(self, dirtstr):
		self.dirtstr = dirtstr

		if dirtstr == self.empty:
			finalstr = 'none'
			return finalstr

		else:
			finalstr = dirtstr
			cleanstr = finalstr.replace('"', '')
			return cleanstr

	def clean_number(self, dirtnum):
		self.dirtnum = dirtnum

		dirtnu = dirtnum.replace('"', '')
		if dirtnu == self.empty: 
			finalnum = 0
			return finalnum

		elif not dirtnu.isnumeric():
			finalnum = 0
			return finalnum
		else:
			finalnum = dirtnum
			cleannum = int(finalnum.replace('"', ''))

			return cleannum


	def clean_date(self,dirtdat):
		self.dirtdat = dirtdat

		if "@" in dirtdat:
			finaldat = '2001,JAN,01'
			return finaldat

		elif "(" in dirtdat:
			finaldat = '2001,JAN,01'
			return finaldat

		elif "-" in dirtdat:
			format_dat = '"%d-%b-%y"'
			dat_str = dirtdat
			date_time_obj = datetime.strptime(dat_str, format_dat)
			out_for = '%Y-%b-%d'
			date_out = datetime.strftime(date_time_obj, out_for)

			return date_out


		else:
			finaldat = '2001,JAN,01'
			return finaldat





		 