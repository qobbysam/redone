#!/usr/bin/python
import psycopg2
import os
from datetime import datetime
import fileinput
from csv import reader

from config import config
from helpers.cleanops import CleanDirty

BASE_DIR = os.getcwd()

FILE_PATH = os.path.join(BASE_DIR,'violations', 'Inspection_2020Apr')

Purge = CleanDirty()
purge_string = Purge.clean_string
purge_number = Purge.clean_number
purge_date = Purge.clean_date



def create_tables():

	line_cont = 0

	try: 
		params = config()

		conn = psycopg2.connect(**params)

		cur = conn.cursor()

		
		insert_into_db = """

							INSERT INTO inspections VALUES 

							(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);

						  """
		
		with open(FILE_PATH+"/2020Apr_Inspection.txt") as fileobject:
			
			for line in fileobject:

				n = line.rstrip()
				m = n.split(",")

				ind = m

				ui = purge_number(ind[0])
				rn = purge_number(ind[1])
				rs = purge_string(ind[2])
				dn = purge_number(ind[3])
				isd = purge_date(ind[4])
				isdl = purge_number(ind[5])
				ccs = purge_string(ind[6])
				tw = purge_number(ind[7])
				dot = purge_number(ind[8])
				vot = purge_number(ind[9])
				ths = purge_number(ind[10])
				ot = purge_number(ind[11])	
				hot = purge_number(ind[12])
				hpr = purge_string(ind[13])
				utd = purge_string(ind[14])
				um = purge_string(ind[15])
				ul = purge_string(ind[16])
				uls = purge_string(ind[17])
				vn = purge_string(ind[18])
				udn = purge_string(ind[19])
				utd2 = purge_string(ind[20])
				um2 = purge_string(ind[21])
				ul2 = purge_string(ind[22])
				uls2 = purge_string(ind[23])
				vn2 = purge_string(ind[24])
				udn2 = purge_string(ind[25])
				usi = purge_string(ind[26])
				fi = purge_string(ind[27])
				dfi = purge_string(ind[28])
				sai = purge_string(ind[29])
				vmi = purge_string(ind[30])
				hi = purge_string(ind[31])
				bv = purge_number(ind[32])
				uv = purge_number(ind[33])
				fv = purge_number(ind[34])
				dfv = purge_number(ind[35])
				sav = purge_number(ind[36])
				vmv = purge_number(ind[37])
				hv = purge_number(ind[38])


				if line_cont == 0:

					print ("title here")

					line_cont += 1

				else:
					



					#print (ind)




					data = (ui,rn,rs,dn,isd,isdl,ccs,tw,dot,vot,ths,ot,hot,hpr,utd,um,ul,uls,vn,udn,utd2,um2,ul2,uls2,vn2,udn2,usi,fi,dfi,sai,vmi,hi,bv,uv,fv,dfv,sav,vmv,hv)

					#cur.execute(insert_into_db, data)
					
					
					#print (data)
					# if yt == 'A':
						#print "can do a now"
					print (data)
					#print (vn)
					cur.execute(insert_into_db, data)
					# else:
					# 	print "not doing a now"

					conn.commit()


	except (Exception, psycopg2.DatabaseError) as error:
		print(error)


	finally:
		if conn is not None:
			conn.close()

if __name__ == '__main__':
	create_tables() 

